/**
 * Simple expression interpreter in Rust.
 */

extern crate rustc_serialize;

// Expression DTO and helper methods.
pub mod expr_dto {

    #[derive(Debug,Clone,RustcEncodable)]
    pub enum ExprType {
        Function,
        Text,
        Boolean
    }
    
    #[derive(Debug,Clone,RustcEncodable)]
    pub enum ValueType {
        Text,
        Boolean
    }
    
    // Note: More idiomatic would be to use enum here
    #[derive(Debug,Clone,RustcEncodable)]
    pub struct Expr {
        etype: ExprType,
        value: String,
        arg: Option<Vec<Expr>>
    }
    
    impl Expr {
        pub fn get_value(&self) -> &String {
            &self.value
        }

        pub fn get_type(&self) -> &ExprType {
            &self.etype
        }
        
        pub fn get_arg(&self) -> &Option<Vec<Expr>> {
            &self.arg
        }
    }

    // Helper functions for building expressions in code
    
    pub fn text(s: String) -> Expr {
        Expr {
            etype: ExprType::Text,
            value: s,
            arg: None
        }
    }
    
    pub fn boolean(b: bool) -> Expr {
        Expr {
            etype: ExprType::Boolean,
            value: b.to_string(),
            arg: None
        }
    }
    
    pub fn function(name: &str, arg: Vec<Expr>) -> Expr {
        Expr {
            etype: ExprType::Function,
            value: name.to_owned(),
            arg: Some(arg)
        }
    }

}

// Expression interpreter and function definitions.
pub mod expr_engine {
    use std::collections::HashMap;
    use expr_dto::*;
    use std::fmt;

    pub type FunctionHandler = Fn(Vec<Expr>) -> Expr + 'static;
    
    pub struct FunctionDef {
        name: String,
        vtype: ValueType,
        handler: Box<FunctionHandler>
    }
    
    pub struct Interpreter {
        functions: HashMap<String, FunctionDef>
    }
    
    impl Interpreter {
        
        pub fn new() -> Interpreter {
            Interpreter {
                functions: HashMap::new()
            }
        }

        // Register a function with the interpreter.
        pub fn add_function (
            &mut self,
            name: &str,
            vtype: ValueType,
            handler: Box<FunctionHandler>
        ) {
            let f = FunctionDef {
                name: name.to_owned(),
                vtype: vtype,
                handler: handler
            };
            self.functions.insert(name.to_owned(), f);
        }

        // Evaluate an expression and return the result.
        pub fn evaluate(&self, e: &Expr) -> Expr {
            use expr_dto::ExprType::*;
            
            let result = match *e.get_type() {
                Text | Boolean => e.clone(),
                Function => {

                    let mut args: Vec<Expr> = Vec::new();

                    // Evaluate arguments and collect
                    match *e.get_arg() {
                        Some(ref arg) => {
                            for a in arg {
                                let result = self.evaluate(a);
                                args.push(result);
                            }
                        },
                        None => {}
                    };
                    
                    // Lookup function in map
                    let name = e.get_value();
                    let fn_def = self.functions.get(name).expect("function does not exist");
                    
                    // Run handler and return result
                    (fn_def.handler)(args)
                }
            };

            result
        }

    }
}

fn main() {
    use expr_dto::*;
    use expr_engine::*;
    use rustc_serialize::json;

    // Create boolean expression like
    // ("Hello" + " world!") == "Hello world!"
    let e0 = function("equals", vec![
        function("concat", vec![
            text("Hello".to_owned()),
            text(" world!".to_owned())
        ]),
        text("Hello world!".to_owned())
    ]);

    // Create interpreter. Need to register some functions,
    // then we can start evaluating expressions.
    let mut i = Interpreter::new();
    
    i.add_function("equals", ValueType::Boolean, Box::new(move |args: Vec<Expr>| {
        
        // Extract LHS = RHS
        let lhs = &args[0];
        let rhs = &args[1];

        // Should we have special handling for other types?
        boolean(lhs.get_value() == rhs.get_value())
    }));
    
    i.add_function("concat", ValueType::Text, Box::new(move |args: Vec<Expr>| {
        
        let mut buf = String::new();

        // Concatenate string args
        // Must be more efficient way to do this?
        for e in args {
            buf = buf + e.get_value();
        }

        text(buf)
    }));

    println!("\nInput:\n{}\n", json::as_pretty_json(&e0));
        
    let e1 = i.evaluate(&e0);
    
    println!("Output:\n{}\n", json::as_pretty_json(&e1));
}
