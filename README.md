# Expression Interpreter in Rust

Simple expression interpreter written to learn Rust.

Install Rust 1.6.0 and then do `cargo run`.

    $ cargo run
       Compiling exprs v0.1.0 (file:///home/tom/wkspace/exprs)
    src/main.rs:79:9: 79:17 warning: unused import, #[warn(unused_imports)] on by default
    src/main.rs:79     use std::fmt;
                           ^~~~~~~~
    src/main.rs:84:9: 84:21 warning: struct field is never used: `name`, #[warn(dead_code)] on by default
    src/main.rs:84         name: String,
                           ^~~~~~~~~~~~
    src/main.rs:85:9: 85:25 warning: struct field is never used: `vtype`, #[warn(dead_code)] on by default
    src/main.rs:85         vtype: ValueType,
                           ^~~~~~~~~~~~~~~~
         Running `target/debug/exprs`

    Input:
    {
      "etype": "Function",
      "value": "equals",
      "arg": [
        {
          "etype": "Function",
          "value": "concat",
          "arg": [
            {
              "etype": "Text",
              "value": "Hello",
              "arg": null
            },
            {
              "etype": "Text",
              "value": " world!",
              "arg": null
            }
          ]
        },
        {
          "etype": "Text",
          "value": "Hello world!",
          "arg": null
        }
      ]
    }

    Output:
    {
      "etype": "Boolean",
      "value": "true",
      "arg": null
    }
